# GitLab CI template for GitLab Package

This project implements a GitLab CI/CD template to publish artifacts to GitLab's [Generic Package Registry](https://docs.gitlab.com/ee/user/packages/generic_packages/).

## Usage

This template can be used both as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component-in-a-cicd-configuration) 
or using the legacy [`include:project`](https://docs.gitlab.com/ee/ci/yaml/index.html#includeproject) syntax.

### Use as a CI/CD component

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the component
  - component: gitlab.com/to-be-continuous/gitlab-package/gitlab-ci-gitlab-package@1.2.1
    # 2: set/override component inputs
    inputs:
      files: "build/*.tgz" # ⚠ this is only an example
```

### Use as a CI/CD template (legacy)

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the template
  - project: 'to-be-continuous/gitlab-package'
    ref: '1.2.1'
    file: '/templates/gitlab-ci-gitlab-package.yml'

variables:
  # 2: set/override template variables
  GLPKG_FILES: "build/*.tgz" # ⚠ this is only an example
```

## Global configuration

The GitLab Package template uses some global configuration used throughout all jobs.

| Input / Variable | Description                            | Default value     |
| --------------------- | -------------------------------------- | ----------------- |
| `image` / `GLPKG_IMAGE` | The Docker image used to publish GitLab packages  | `registry.hub.docker.com/curlimages/curl:latest` |

## Jobs

### `glpkg-publish` job

This job **publishes** files to [GitLab Generic Package](https://docs.gitlab.com/ee/user/packages/generic_packages/).

This job is bound to the `publish` stage and is executed on a Git tag with a semantic version pattern (`v?[0-9]+\.[0-9]+\.[0-9]+`, _configurable_).

It uses the following variables:

| Input / Variable | Description                              | Default value     |
| --------------------- | ---------------------------------------- | ----------------- |
| `files` / `GLPKG_FILES` | Glob patterns matching files to include in the GitLab package (:warning: does not support double star). | _none_ (mandatory) |
| `package` / `GLPKG_PACKAGE` | Name of the package to publish           | `$CI_PROJECT_NAME` |
