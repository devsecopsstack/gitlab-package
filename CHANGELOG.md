## [1.2.1](https://gitlab.com/to-be-continuous/gitlab-package/compare/1.2.0...1.2.1) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([04aca2e](https://gitlab.com/to-be-continuous/gitlab-package/commit/04aca2e0a457bf2fe0121ee25e3df0caeb099a75))

# [1.2.0](https://gitlab.com/to-be-continuous/gitlab-package/compare/1.1.0...1.2.0) (2024-1-27)


### Features

* migrate to CI/CD component ([30cb585](https://gitlab.com/to-be-continuous/gitlab-package/commit/30cb585e4eca3d7ab1a11b0b350fe0eaab734fce))

# [1.1.0](https://gitlab.com/to-be-continuous/gitlab-package/compare/1.0.0...1.1.0) (2023-12-8)


### Features

* use centralized tracking image (gitlab.com) ([b847266](https://gitlab.com/to-be-continuous/gitlab-package/commit/b84726630d6dc9339c8f4845b701c8025f41abd9))
